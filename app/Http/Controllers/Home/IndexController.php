<?php

namespace App\Http\Controllers\Home;


use App\Http\Model\Article;
use App\Http\Model\Category;

use Illuminate\Support\Facades\Config;

class IndexController extends CommonController
{
    public function index(){
        $title=Config::get('web.web_title');
        $key='各类新闻';
        $description='为全球用户24小时提供全面及时的中文资讯，内容覆盖国内外突发新闻事件、体坛赛事、娱乐时尚、产业资讯、实用信息等，设有新闻、体育、娱乐、财经、科技、房产、汽车等30多个内容频道，同时开设博客、视频、论坛等自由互动交流空间。';
        /*点击量最高的6文章*/
        $hot6=Article::orderBy('art_views','desc')->take(6)->get();
        /*图文列表分页*/
        $data=Article::orderBy('art_time','desc')->paginate(5);
        return view('home.index',compact('title','key','description','hot6','data'));
    }

    public function artlist($cat_id){
        $field=Category::find($cat_id);
        $title=$field->cat_title;
        $key=$field->cat_keywords;
        $description=$field->cat_description;
        /*子分类*/
        $subcate=Category::where('cat_pid',$cat_id)->get();
        /*分类文章*/
        $data=Article::orderBy('art_time','desc')->where('art_cat_id',$cat_id)->paginate(5);
        return view('home.list',compact('title','key','description','field','subcate','data'));
    }

    public function article($art_id){
        /*查询文章*/
        $article=Article::join('category','category.cat_id','=','article.art_cat_id')->where('art_id',$art_id)->first();
        $title=$article->art_title;
        $key=$article->art_tag;
        $description=$article->art_description;
        $arts['pre']=Article::where('art_id','<',$art_id)->orderBy('art_id','desc')->first();
        $arts['next']=Article::where('art_id','>',$art_id)->orderBy('art_id','asc')->first();
        /*相关文章*/
        $related=Article::where('art_cat_id',$article->cat_id)->take(6)->get();
        /*更新查看次数*/
        Article::where('art_id',$art_id)->increment('art_views');
        Category::where('cat_id',$article->cat_id)->increment('cat_views');
        return view('home.article',compact('title','key','description','article','arts','related'));
    }
}
