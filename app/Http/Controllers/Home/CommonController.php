<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Model\Navs;
use App\Http\Model\Links;
use App\Http\Model\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class CommonController extends Controller
{
    public function __construct(){
        $navs=Navs::orderBy('nav_order','asc')->get();
        /*点击量最高的5文章*/
        $hot5=Article::orderBy('art_views','desc')->take(5)->get();
        /*8条最新文章*/
        $new=Article::orderBy('art_time','desc')->take(8)->get();
        /*友情连接*/
        $links=Links::orderBy('link_order','asc')->get();
        View::share('navs',$navs);
        View::share('hot5',$hot5);
        View::share('new',$new);
        View::share('links',$links);
    }
}
