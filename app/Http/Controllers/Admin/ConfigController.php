<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\Config;
use Faker\Provider\File;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ConfigController extends CommonController
{
    /*全部配置列表*/
    public function index(){
        $data=Config::orderBy('conf_order','desc')->get();
        foreach($data as $k => $v){
            switch($v['field_type']){
                case 'input':
                    $data[$k]['html']="<input type='text' class='lg' name='conf_value[".$v['conf_id']."]' value='".$v['conf_value']."'>";
                    break;
                case 'radio':
                    $data[$k]['html']='';
                    $arr=explode(',',$v['field_value']);
                    foreach($arr as $k1 => $v1){
                        $arr1=explode('|',$v1);
                        $c=$arr1[0]==$v['conf_value']?'checked':'';
                        $data[$k]['html'].=$arr1[1]."：<input type='radio' ".$c." name='conf_value[".$v['conf_id']."]' value='".$arr1[0]."'> &nbsp&nbsp ";
                    }
                    break;
                case 'textarea':
                    $data[$k]['html']="<textarea name='conf_value[".$v['conf_id']."]'>".$v['conf_value']."</textarea>";
                    break;
            }
        }
        return view('admin.config.index',compact('data'));
    }
    /*添加*/
    public function create(){
        return view('admin.config.add');
    }
    /*添加处理提交位置*/
    public function store(){
        $inputData =Input::except('_token');
        $rules=[
            'conf_title'=>'required',
            'conf_name'=>'required',
            'conf_tips'=>'required',
            'field_type'=>'required',
        ];
        $message=[
            'conf_title.required'=>'配置标题不能为空',
            'conf_name.required'=>'配置名称不能为空',
            'conf_tips.required'=>'配置说明不能为空',
            'field_type.required'=>'字段类型不能为空',
        ];
        $validator=Validator::make($inputData,$rules,$message);
        if($validator->passes()){
            $res= Config::create($inputData);
            if($res){
                return redirect('admin/config');
            }else{
                return back()->withErrors(['errors'=>'保存失败']);
            }
        }else{
            $errors=$validator->errors()->all();
            return back()->withErrors($errors);
        }
    }
    
    /*编辑文章*/
    public function edit($conf_id){
        $conf=Config::find($conf_id);
        return view('admin/config/edit',compact('conf'));
    }
    /*修改提交*/
    public function update($conf_id){
        /*不接受这两个字段*/
        $updata=Input::except('_method','_token');
        $rules=[
            'conf_title'=>'required',
            'conf_name'=>'required',
            'conf_tips'=>'required',
            'field_type'=>'required',
        ];
        $message=[
            'conf_title.required'=>'配置标题不能为空',
            'conf_name.required'=>'配置名称不能为空',
            'conf_tips.required'=>'配置说明不能为空',
            'field_type.required'=>'字段类型不能为空',
        ];
        $validator=Validator::make($updata,$rules,$message);
        if($validator->passes()){
            $res=Config::where('conf_id',$conf_id)->update($updata);
            if($res){
                $this->putConfig();
                return redirect('admin/config');
            }else{
                return back()->withErrors(['errors'=>'更新失败']);
            }
        }else{
            $errors=$validator->errors()->all();
            return back()->withErrors($errors);
        }
    }

    /*删除配置*/
    // delete.admin/config/{category}
    public function destroy($conf_id){
        /*删除*/
        $res=Config::where('conf_id',$conf_id)->delete();
        if($res){
            $rdata=[
                'status'=>1,
                'msg'=>'删除成功'
            ];
            $this->putConfig();
        }else{
            $rdata=[
                'status'=>0,
                'msg'=>'删除失败'
            ];
        }
        return $rdata;
    }
    //    ?排序
    public function changeOrder(){
        $postData=Input::all();
        $conf=Config::find($postData['conf_id']);
        $conf->conf_order=$postData['conf_order'];
        $res=$conf->update();
        if($res){
            $rdata=[
                'status'=>1,
                'msg'=>'更新成功',
            ];
        }else{
            $rdata=[
                'status'=>0,
                'msg'=>'更新失败',
            ];
        }
        return $rdata;
    }

    /*修改配置值*/
    public function chConfigVal(){
        $data=Input::except('_token');
        foreach($data['conf_value'] as $k => $v){
            Config::where('conf_id',$k)->update(['conf_value'=>$v]);
        }
        $this->putConfig();
        return back()->withErrors(['errors'=>'修改完成']);
    }

    /*写入配置文件*/
    public function putConfig(){
        /*获取配置信息*/
//        echo \Illuminate\Support\Facades\Config::get('web.不知道');
        $data=Config::pluck('conf_value','conf_name')->all();
        $path=base_path().'\config\web.php';
        $str='<?php
         return '.var_export($data,true).';';
        file_put_contents($path,$str);
    }

    /*备份数据库*/
    public function backDB(){
        $shell="mysqldump -uroot -p9359 larblog >D:\\phpStudy\\WWW\\larblog\\DB_back\\".date('Y_m_d_H_i_s').".sql";
        exec($shell);
        return [
            'status'=>1,
            'msg'=>'备份成功'
        ];
    }
}
