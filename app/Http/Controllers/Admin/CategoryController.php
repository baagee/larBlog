<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class CategoryController extends CommonController
{
    // admin/category全部分类列表
    public function index(){
//        $categories=Category::all()->all();
        $data=(new Category)->getData();
        return view('admin/category/index')->with('data',$data);
    }

    // GET|HEAD 添加分类
    public function create(){
        /*获取父级分类数据*/
        $topCate=Category::where('cat_pid',0)->get();

        return view('admin/category/add',compact('topCate'));
    }
    // admin/category  添加分类提交方法
    public function store(){
        $inputData=Input::except('_token');
        /*验证规则*/
        $rules=[
            'cat_name'=>'required',
            'cat_title'=>'required',
        ];
        /*设置自定义错误提示信息*/
        $message=[
            'cat_name.required'=>'标题不能为空',
            'cat_title.required'=>'分类名称不能为空',
        ];
        $validator=Validator::make($inputData,$rules,$message);
        if($validator->passes()){
            /*添加数据*/
            $res=Category::create($inputData);
            if($res){
                return redirect('admin/category');
            }else{
                return back()->withErrors(['errors'=>'添加失败']);
            }
        }else{
            $errors=$validator->errors()->all();
            return back()->withErrors($errors);
        }
    }

    // admin/category/{category}  显示单个分类信息
    public function show(){

    }

    // delete.admin/category/{category}  删除单个分类
    public function destroy($cat_id){
        /*删除*/
        $res=Category::where('cat_id',$cat_id)->delete();
        if($res){
            /*修改子分类为顶级分类*/
            $subCate=Category::where('cat_pid',$cat_id)->update(['cat_pid'=>0]);
            if($subCate){
                $rdata=[
                    'status'=>1,
                    'msg'=>'删除成功'
                ];
            }else{
                $rdata=[
                    'status'=>1,
                    'msg'=>'删除成功,但是子分类未作修改'
                ];
            }
        }else{
            $rdata=[
                'status'=>0,
                'msg'=>'删除失败'
            ];
        }
        return $rdata;
    }

    // admin/category/{category}  更新一个分类
    public function update($cat_id){
        /*不接受这两个字段*/
        $updata=Input::except('_method','_token');
        /*验证规则*/
        $rules=[
            'cat_name'=>'required',
            'cat_title'=>'required',
        ];
        /*设置自定义错误提示信息*/
        $message=[
            'cat_name.required'=>'标题不能为空',
            'cat_title.required'=>'分类名称不能为空',
        ];
        $validator=Validator::make($updata,$rules,$message);
        if($validator->passes()){
            $res=Category::where('cat_id',$cat_id)->update($updata);
            if($res){
                return redirect('admin/category');
            }else{
                return back()->withErrors(['errors'=>'更新失败']);
            }
        }else{
            $errors=$validator->errors()->all();
            return back()->withErrors($errors);
        }
    }
    // admin/category/{category}/edit  编辑分类
    public function edit($cat_id){
        $oneCate=Category::find($cat_id);
        $topCate=Category::where('cat_pid',0)->get();
        return view('admin/category/edit',compact('oneCate','topCate'));
    }

    public function changeOrder(){
        $postData=Input::all();
        $cate=Category::find($postData['cat_id']);
        $cate->cat_order=$postData['cat_order'];
        $res=$cate->update();
        if($res){
            $rdata=[
                'status'=>1,
                'msg'=>'更新成功',
            ];
        }else{
            $rdata=[
              'status'=>0,
                'msg'=>'更新失败',
            ];
        }
        return $rdata;
    }
}