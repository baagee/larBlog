<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class CommonController extends Controller{
    /*上传图片*/
    public function upload(){
        $imgFile=Input::file('Filedata');
        if($imgFile->isValid()){
            /*获取文件后缀名*/
            $extension=$imgFile->getClientOriginalExtension();
            $newName=date('Y_m_d_H_i_s').mt_rand(100,900).'.'.$extension;
            /*移动文件*/
            $path=$imgFile->move(base_path().'\uploads\thumbImg',$newName);
            $filePath='uploads/thumbImg/'.$newName;
            return $filePath;
        }
    }
}
