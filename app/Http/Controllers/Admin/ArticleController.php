<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\Article;
use App\Http\Model\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ArticleController extends CommonController
{
    /*全部文章列表*/
    public function index(){
        $cat_id=$tag=$title='';
        $cates=(new Category)->getData();
        $data=Article::select(
            'article.art_id','article.art_title','article.art_thumb','article.art_tag',
            'article.art_views','article.art_editor','article.art_time','category.cat_name')
            ->join('category','cat_id','=','art_cat_id','left')->orderBy('art_id','desc');
        if($search=Input::except('page')){
            $cat_id=$search['art_cat_id'];
            $tag=$search['art_tag'];
            $title=$search['art_title'];
            if(!empty($cat_id)){
                /*希望查找子分类的数据*/
                $subCates=Category::where('cat_pid',$cat_id)->select('cat_id')->get()->all();
                $inCate=[intval($cat_id)];
                if(!empty($subCates)){
                    foreach($subCates as $v){
                        $inCate[]=$v['cat_id'];
                    }
                }
                /*whereIn('id', array(1, 2, 3))*/
                $data=$data->whereIn('art_cat_id',$inCate);
            }
            if(!empty($tag)){
                $data=$data->where('art_tag','like','%'.$tag.'%');
            }
            if(!empty($title)){
                $data=$data->where('art_tag','like','%'.$title.'%');
            }
            $data=$data->paginate(2)->appends(['art_cat_id' => $cat_id,'art_tag'=>$tag,'art_title'=>$title]);
        }else{
            $data=$data->paginate(3);
        }
        return view('admin.article.index',compact('data','cates','cat_id','tag','title'));
    }
    /*添加*/
    public function create(){
        $cates=(new Category)->getData();
        return view('admin/article/add',compact('cates'));
    }
    /*添加处理提交位置*/
    public function store(){
        $inputData =Input::except('_token');
        $rules=[
            'art_title'=>'required',
            'art_tag'=>'required',
            'art_thumb'=>'required',
            'art_content'=>'required',
            'art_cat_id'=>'required',
        ];
        $message=[
            'art_title.required'=>'标题不能为空',
            'art_tag.required'=>'标签不能为空',
            'art_thumb.required'=>'缩略图不能为空',
            'art_content.required'=>'文章主体不能为空',
            'art_cat_id.required'=>'文章分类不能为空',
        ];
        $validator=Validator::make($inputData,$rules,$message);
        if($validator->passes()){
            $inputData['art_time']=time();
//          dd($inputData);
            $res= Article::create($inputData);
            if($res){
                return redirect('admin/article');
            }else{
                return back()->withErrors(['errors'=>'保存失败']);
            }
        }else{
            $errors=$validator->errors()->all();
            return back()->withErrors($errors);
        }
    }
    
    /*编辑文章*/
    public function edit($art_id){
        $article=Article::find($art_id);
        $cates=(new Category)->getData();
        return view('admin/article/edit',compact('article','cates'));
    }
    /*修改提交*/
    public function update($art_id){
        /*不接受这两个字段*/
        $updata=Input::except('_method','_token');
        $rules=[
            'art_title'=>'required',
            'art_tag'=>'required',
            'art_thumb'=>'required',
            'art_content'=>'required',
            'art_cat_id'=>'required',
        ];
        $message=[
            'art_title.required'=>'标题不能为空',
            'art_tag.required'=>'标签不能为空',
            'art_thumb.required'=>'缩略图不能为空',
            'art_content.required'=>'文章主体不能为空',
            'art_cat_id.required'=>'文章分类不能为空',
        ];
        $validator=Validator::make($updata,$rules,$message);
        if($validator->passes()){
            $res=Article::where('art_id',$art_id)->update($updata);
            if($res){
                return redirect('admin/article');
            }else{
                return back()->withErrors(['errors'=>'更新失败']);
            }
        }else{
            $errors=$validator->errors()->all();
            return back()->withErrors($errors);
        }
    }

    /*删除文章*/
    // delete.admin/article/{category}
    public function destroy($art_id){
        $imgPath=Article::select('article.art_thumb')->find($art_id);
        /*删除*/
        $res=Article::where('art_id',$art_id)->delete();
        if($res){
            /*删除图片*/
            if(!empty($imgPath['art_thumb']) && file_exists(str_replace('\\','/',base_path()).'/'.$imgPath['art_thumb'])){
                $deleteImg=unlink(str_replace('\\','/',base_path()).'/'.$imgPath['art_thumb']);
                if($deleteImg){

                }else{
                    $rdata=[
                        'status'=>1,
                        'msg'=>'删除成功,但是图片未删除'
                    ];
                    return $rdata;
                }
            }
            $rdata=[
                'status'=>1,
                'msg'=>'删除成功'
            ];
            return $rdata;
        }else{
            $rdata=[
                'status'=>0,
                'msg'=>'删除失败'
            ];
            return $rdata;
        }
    }
}
