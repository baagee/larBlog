<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class LinksController extends CommonController
{
    /*全部连接列表*/
    public function index(){
        $links=Links::orderBy('link_order','desc')->get();
        return view('admin.links.index',compact('links'));
    }
    /*排序*/
    public function changeOrder(){
        $postData=Input::all();
        $link=Links::find($postData['link_id']);
        $link->link_order=$postData['link_order'];
        $res=$link->update();
        if($res){
            $rdata=[
                'status'=>1,
                'msg'=>'更新成功',
            ];
        }else{
            $rdata=[
                'status'=>0,
                'msg'=>'更新失败',
            ];
        }
        return $rdata;
    }

    /*添加*/
    public function create(){
        return view('admin/links/add');
    }

    /*添加处理提交位置*/
    public function store()
    {
        $inputData = Input::except('_token');
        $rules = [
            'link_title' => 'required',
            'link_name' => 'required',
            'link_url' => 'required',
        ];
        $message = [
            'link_title.required' => '标题不能为空',
            'link_name.required' => '名称不能为空',
            'link_url.required' => '链接地址不能为空',
        ];
        $validator = Validator::make($inputData, $rules, $message);
        if ($validator->passes()) {
            $res=Links::create($inputData);
            if($res){
                return redirect('admin/links');
            }else{
                return back()->withErrors(['errors'=>'保存失败']);
            }
        }else{
            $errors=$validator->errors()->all();
            return back()->withErrors($errors);
        }
    }

    /*编辑文章*/
    public function edit($link_id){
        $link=Links::find($link_id);
        return view('admin/links/edit',compact('link'));
    }
    /*修改提交*/
    public function update($link_id){
        $link=Input::except('_token','_method');
        $rules = [
            'link_title' => 'required',
            'link_name' => 'required',
            'link_url' => 'required',
        ];
        $message = [
            'link_title.required' => '标题不能为空',
            'link_name.required' => '名称不能为空',
            'link_url.required' => '链接地址不能为空',
        ];
        $validator = Validator::make($link, $rules, $message);
        if($validator->passes()){
            $res=Links::where('link_id',$link_id)->update($link);
            if($res){
                return redirect('admin/links');
            }else{
                return back()->withErrors(['errors'=>'修改失败']);
            }
        }else{
            $errors=$validator->errors()->all();
            return back()->withErrors($errors);
        }
    }
    /*删除*/
    public function destroy($link_id){
        $res=Links::where('link_id',$link_id)->delete();
        if($res){
            $rdata=[
                'status'=>1,
                'msg'=>'删除成功'
            ];
        }else{
            $rdata=[
                'status'=>0,
                'msg'=>'删除失败'
            ];
        }
        return $rdata;
    }
}
