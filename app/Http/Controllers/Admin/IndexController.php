<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class IndexController extends CommonController{
    public function index(){
        return view('admin.welcome');
    }
    public function info(){
        return view('admin.info');
    }
    /*修改密码*/
    public function chpass(){
        if($postData=Input::all()){
            /*验证规则*/
            $rules=[
                'password'=>'required|between:6,20|confirmed',
            ];
            /*设置自定义错误提示信息*/
            $message=[
                'password.required'=>'新密码不能为空',
                'password.between'=>'新密码必须在6-20位之间',
                'password.confirmed'=>'两个密码不一样'
            ];
            $validator=Validator::make($postData,$rules,$message);
            if($validator->passes()){
                $user=User::first();
                $password=Crypt::decrypt($user->password);
                if($password!=md5($postData['password_o'])){
                    return back()->withErrors(['errors'=>'原密码错误']);
                }else{
                    /*更新密码*/
                    $user->password=Crypt::encrypt(MD5($postData['password']));
                    $res=$user->update();
                    if($res){
//                        return redirect('admin/info');
                        return back()->withErrors(['errors'=>'修改密码成功']);
                    }else{
                        return back()->withErrors(['errors'=>'修改失败']);
                    }
                }
           }else{
                $err=$validator->errors()->all();
                return back()->withErrors($err);
            }
        }else {
            return view('admin.chpass');
        }
    }
}
