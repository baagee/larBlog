<?php

namespace App\Http\Controllers\Admin;

use App\Http\Model\Navs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class NavsController extends CommonController
{
    /*全部连接列表*/
    public function index(){
        $navs=Navs::orderBy('nav_order','desc')->get();
        return view('admin.navs.index',compact('navs'));
    }
    /*排序*/
    public function changeOrder(){
        $postData=Input::all();
        $nav=Navs::find($postData['nav_id']);
        $nav->nav_order=$postData['nav_order'];
        $res=$nav->update();
        if($res){
            $rdata=[
                'status'=>1,
                'msg'=>'更新成功',
            ];
        }else{
            $rdata=[
                'status'=>0,
                'msg'=>'更新失败',
            ];
        }
        return $rdata;
    }

    /*添加*/
    public function create(){
        return view('admin/navs/add');
    }

    /*添加处理提交位置*/
    public function store()
    {
        $inputData = Input::except('_token');
        $rules = [
            'nav_alias' => 'required',
            'nav_name' => 'required',
            'nav_url' => 'required',
        ];
        $message = [
            'nav_alias.required' => '别名不能为空',
            'nav_name.required' => '名称不能为空',
            'nav_url.required' => '链接地址不能为空',
        ];
        $validator = Validator::make($inputData, $rules, $message);
        if ($validator->passes()) {
            $res=Navs::create($inputData);
            if($res){
                return redirect('admin/navs');
            }else{
                return back()->withErrors(['errors'=>'保存失败']);
            }
        }else{
            $errors=$validator->errors()->all();
            return back()->withErrors($errors);
        }
    }

    /*编辑文章*/
    public function edit($nav_id){
        $nav=Navs::find($nav_id);
        return view('admin/navs/edit',compact('nav'));
    }
    /*修改提交*/
    public function update($nav_id){
        $nav=Input::except('_token','_method');
        $rules = [
            'nav_alias' => 'required',
            'nav_name' => 'required',
            'nav_url' => 'required',
        ];
        $message = [
            'nav_alias.required' => '别名不能为空',
            'nav_name.required' => '名称不能为空',
            'nav_url.required' => '链接地址不能为空',
        ];
        $validator = Validator::make($nav, $rules, $message);
        if($validator->passes()){
            $res=Navs::where('nav_id',$nav_id)->update($nav);
            if($res){
                return redirect('admin/navs');
            }else{
                return back()->withErrors(['errors'=>'修改失败']);
            }
        }else{
            $errors=$validator->errors()->all();
            return back()->withErrors($errors);
        }
    }
    /*删除*/
    public function destroy($nav_id){
        $res=Navs::where('nav_id',$nav_id)->delete();
        if($res){
            $rdata=[
                'status'=>1,
                'msg'=>'删除成功'
            ];
        }else{
            $rdata=[
                'status'=>0,
                'msg'=>'删除失败'
            ];
        }
        return $rdata;
    }
}
