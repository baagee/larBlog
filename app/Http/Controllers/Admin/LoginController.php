<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Model\User;
use App\Http\Requests;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;

/*加载验证码类*/
require_once 'resources/extends/verify/Code.class.php';
class LoginController extends CommonController{
    public function login(){
        if($logData=Input::all()){
            $verify = new \Code();
            $verifyCode=$verify->get();
            if(strtoupper($logData['verify'])!=$verifyCode){
                return back()->with('msg','验证码错误');//返回前一页
            }
/*            $passmd5=md5($logData['password']);
            Crypt::encrypt($passmd5);*/
            $user=User::first();
            if($user['username']!=$logData['username'] || Crypt::decrypt($user['password'])!=md5($logData['password'])){
                /*将错误信息保存到session*/
                return back()->with('msg','用户名或者密码错误');
            }
            /*登陆成功加入session保存*/
            session(['user'=>$user]);
            return redirect('admin');
        }else{
            return view('Admin.login');
        }
    }
    /*验证码*/
    public function verify(){
        $verify = new \Code();
        $verify->make();
    }

    /*退出*/
    public function logout(){
        session(['user'=>'']);
        return redirect('admin/login');
    }



/*    public function crypt(){
//加密测试
        $csrf=md5('qazplm098');
        $a='eyJpdiI6Im83MzRTcDVvWmRLY3JnT3REK29wcHc9PSIsInZhbHVlIjoidkpISVwvb01udlBZU0ZkVXZrOXZJSUJOSEZXWDBIUHVWZHFZSU5Bem1TdldIUGFqNFJ3MmdFMjlrTlBLQ0hXb1UiLCJtYWMiOiIyNzM3NTkwNTY5ODJmMzdiMTI3MGY0Y2I2YmY5NmM5MDQzZjY3MjY1YjJlYTliYTQ5ZmJlMWI0Yzk1OTk4MmIzIn0=';
        echo Crypt::encrypt($csrf);
        echo Crypt::decrypt($a);
    }*/
}
