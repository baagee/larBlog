<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware'=>['check.login'],'prefix'=>'admin','namespace'=>'Admin'], function(){
    Route::get('/','IndexController@index');
    Route::get('info','IndexController@info');
    Route::get('logout','LoginController@logout');
    Route::any('chpass','IndexController@chpass');

    /*分类路由*/
    Route::resource('category','CategoryController');
    Route::post('category/changeOrder','CategoryController@changeOrder');
    /*文章路由*/
    Route::post('article/search','ArticleController@search');
    Route::resource('article','ArticleController');
    Route::any('upload','CommonController@upload');
    /*友情链接路由*/
    Route::resource('links','LinksController');
    Route::post('links/changeOrder','LinksController@changeOrder');
    /*菜单路由*/
    Route::resource('navs','NavsController');
    Route::post('navs/changeOrder','NavsController@changeOrder');
    /*写入配置路由*/
    Route::get('config/putConfig','ConfigController@putConfig');
    /*网站配置路由*/
    Route::resource('config','ConfigController');
    Route::post('config/changeOrder','ConfigController@changeOrder');
    /*配置修改*/
    Route::post('config/chConfigVal','ConfigController@chConfigVal');
    /*数据库备份路由*/
    Route::post('database','ConfigController@backDB');
});

//登陆首页
Route::any('admin/login','Admin\LoginController@login');
//验证码路由
Route::get('admin/verify','Admin\LoginController@verify');

//网站首页路由组
Route::group(['namespace'=>'Home'],function(){
    Route::get('/','IndexController@index');
    Route::get('list/{cat_id}','IndexController@artlist');
    Route::get('article/{art_id}','IndexController@article');
});