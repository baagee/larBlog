<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='category';
    protected $primaryKey='cat_id';
    protected $fillable=['cat_name','cat_pid','cat_title','cat_keywords','cat_order','cat_description'];

    public $timestamps=false;

    public function getData(){
        $data=$this->orderBy('cat_order','desc')->get();
        return $this->getTree($data);
    }
    /*树形结构*/
    public function getTree($data){
        $arr=array();
        foreach($data as $k=>$v){
            if($v['cat_pid']==0){
//                $data[$k]['cat_name']='|—'.$data[$k]['cat_name'];
                $arr[]=$data[$k];
                foreach($data as $k1=>$v1){
                    if($v1['cat_pid']==$v['cat_id']){
                        $data[$k1]['cat_name']='|—'.$data[$k1]['cat_name'];
                        $arr[]=$data[$k1];
                    }
                }
            }
        }
        return $arr;
    }
}
