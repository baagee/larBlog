<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table='config';
    protected $primaryKey='conf_id';
    public $timestamps=false;
    protected $fillable=['conf_title','conf_name','conf_value','conf_order','conf_tips','field_type','field_value'];
}
