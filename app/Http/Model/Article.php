<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table='article';
    protected $primaryKey='art_id';
    public $timestamps=false;
    protected $fillable=['art_title','art_cat_id','art_tag','art_editor','art_thumb','art_time','art_description','art_content'];
}
