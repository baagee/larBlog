<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('navs', function (Blueprint $table) {
            $table->engine='MyISAM';
            $table->increments('nav_id');
            $table->string('nav_name')->default('')->comment('菜单链接名称');
            $table->string('nav_alias')->default('')->comment('菜单链接标题');
            $table->string('nav_url')->default('')->comment('菜单链接url');
            $table->integer('nav_order')->default(0)->comment('菜单链接排序');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('navs');
    }
}
