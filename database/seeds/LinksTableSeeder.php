<?php

use Illuminate\Database\Seeder;

class LinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=[
            [
                'link_name' => '你爸爸',
                'link_title' => '你爸爸',
                'link_url' => 'http://www.nibaba.com',
                'link_order' => 1,
            ],
            [
                'link_name' => '你mama',
                'link_title' => '你mama',
                'link_url' => 'http://www.nimama.com',
                'link_order' => 3,
            ],
            [
                'link_name' => '你jiejie',
                'link_title' => '你jiejie',
                'link_url' => 'http://www.nijiejie.com',
                'link_order' => 2,
            ],
            [
                'link_name' => '你didi',
                'link_title' => '你滴滴ie',
                'link_url' => 'http://www.nididiiejie.com',
                'link_order' => 5,
            ],
            [
                'link_name' => '你34653e',
                'link_title' => '你j65867',
                'link_url' => 'http://www.nityrrte.com',
                'link_order' => 4,
            ],
        ];
        DB::table('links')->insert($data);
    }
}
