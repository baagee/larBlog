﻿@extends('home.layout')
@section('content')
<article class="blogs">
  <h1 class="t_nav">
      <a href="/" class="n1">网站首页</a><a href="{{url('list/'.$article->cat_id)}}" class="n2">{{$article->cat_name}}</a></h1>
  <div class="index_about">
    <h2 class="c_titile">{{$article->art_title}}</h2>
    <p class="box_c"><span class="d_time">发布时间：{{date('Y-m-d H:i:s',$article->art_time)}}</span><span>编辑：{{$article->art_editor}}</span><span>查看次数：{{$article->art_views}}</span></p>
    <ul class="infos">
        {!! $article->art_content !!}
    </ul>
    <div class="keybq">
    <p><span>关键字词</span>：{{$article->art_tag}}</p>
    
    </div>
    <div class="ad"> </div>
    <div class="nextinfo">
      <p>上一篇：@if($arts['pre'])
          <a title="{{$arts['pre']['art_title']}}" href="{{url('article/'.$arts['pre']['art_id'])}}">{{$arts['pre']['art_title']}}</a></p>
        @else
            <span>没有了</span>
        @endif
      <p>下一篇：@if($arts['next'])
          <a title="{{$arts['next']['art_title']}}" href="{{url('article/'.$arts['next']['art_id'])}}">{{$arts['next']['art_title']}}</a></p>
        @else
            <span>没有了</span>
        @endif
    </div>
    <div class="otherlink">
      <h2>相关文章</h2>
      <ul>
          @foreach($related as $v)
        <li><a href="{{url('article/'.$v['art_id'])}}" title="{{$v['art_title']}}">{{$v['art_title']}}</a></li>
              @endforeach
      </ul>
    </div>
  </div>
  <aside class="right">
    <div class="blank"></div>
    @parent
  </aside>
</article>

    @endsection