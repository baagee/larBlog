﻿@extends('home.layout')
@section('content')
<div class="banner">
  <section class="box">
    <ul class="texts">
      <p>打了死结的青春，捆死一颗苍白绝望的灵魂。</p>
      <p>为自己掘一个坟墓来葬心，红尘一梦，不再追寻。</p>
      <p>加了锁的青春，不会再因谁而推开心门。</p>
    </ul>
    <div class="avatar"><a href="#"><span>BaAGee</span></a> </div>
  </section>
</div>
<div class="template">
  <div class="box">
    <h3>
      <p><span>最热</span>文章</p>
    </h3>
    <ul>
        @foreach($hot6 as $h)
      <li><a href="{{url('article/'.$h['art_id'])}}"  target="_blank"><img style="height: 110px" src="{{url($h['art_thumb'])}}"></a><span>{{$h['art_title']}}</span></li>
            @endforeach
    </ul>
  </div>
</div>
<article>
  <h2 class="title_tj">
    <p>文章<span>推荐</span></p>
  </h2>

  <div class="bloglist left">
      @foreach($data as $v)
    <h3>{{$v['art_title']}}</h3>
    <figure><img src="{{url($v['art_thumb'])}}"></figure>
    <ul>
      <p>{{$v['art_description']}}</p>
      <a title="{{$v['art_title']}}" href="{{url('article/'.$v['art_id'])}}" target="_blank" class="readmore">阅读全文>></a>
    </ul>
    <p class="dateview"><span> &nbsp;{{date('Y-m-d H:i:s',$v['art_time'])}}</span><span>作者：{{$v['art_editor']}}</span><span>个人博客：[<a href="/">程序人生</a>]</span></p>
          @endforeach
      <div class="page">
          {{$data->links()}}
      </div>
  </div>

  <aside class="right">

    <div class="weather" style="float: left"><iframe width="250" scrolling="no" height="60" frameborder="0" allowtransparency="true" src="http://i.tianqi.com/index.php?c=code&id=12&icon=1&num=1"></iframe></div>

      @parent

    </aside>
</article>

    @endsection