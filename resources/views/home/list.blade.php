﻿@extends('home.layout')
@section('content')
<article class="blogs">
<h1 class="t_nav"><span>{{$field->cat_description}}</span><a href="{{url('/')}}" class="n1">网站首页</a><a href="javascript:;" class="n2">{{$field->cat_name}}</a></h1>
<div class="newblog left">
    @foreach($data as $v)
   <h2>{{$v['art_title']}}</h2>
   <p class="dateview"><span> &nbsp;发布时间：{{date('Y-m-d H:i:s',$v['art_time'])}}</span><span>作者：{{$v['art_editor']}}</span><span>分类：[<a href="{{url('list/'.$field->cat_id)}}">{{$field->cat_name}}</a>]</span></p>
    <figure><img style="height: 110px" src="{{url($v['art_thumb'])}}"></figure>
    <ul class="nlist">
      <p>{{$v['art_description']}}...</p>
      <a title="{{$v['art_title']}}" href="{{url('article/'.$v['art_id'])}}" target="_blank" class="readmore">阅读全文>></a>
    </ul>
    <div class="line"></div>
    @endforeach
    <div class="blank"></div>

    <div class="page">
    {{$data->links()}}
    </div>
</div>
<aside class="right">
    @if($subcate->all())
   <div class="rnav">
      <ul>
          @foreach($subcate as $k=> $s)
       <li class="rnav{{$k+1}}"><a href="{{url('list/'.$s['cat_id'])}}" target="_blank">{{$s['cat_name']}}</a></li>
              @endforeach
     </ul>
    </div>
    @endif

@parent

</aside>
</article>

    @endsection