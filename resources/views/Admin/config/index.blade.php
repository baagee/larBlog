@extends('admin.layouts')
@section('content')
    <!--面包屑导航 开始-->
    <div class="crumb_warp">
        <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
        <i class="fa fa-home"></i> <a href="{{url('admin/info')}}">首页</a>&raquo; 系统配置列表
    </div>
    <!--面包屑导航 结束-->
    <!--搜索结果页面 列表 开始-->
        <div class="result_wrap">
            <div class="result_title">
                <h3>快捷操作</h3>
                @if(count($errors))
                    <div class="mark">
                        @foreach($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    </div>
                @endif
            </div>
            <!--快捷导航 开始-->
            <div class="result_content">
                <div class="short_wrap">
                    <a href="{{url('admin/config/create')}}"><i class="fa fa-plus"></i>新增系统配置</a>
                </div>
            </div>
            <!--快捷导航 结束-->
        </div>
<form action="{{url('admin/config/chConfigVal')}}" method="post">
    {{csrf_field()}}
    <div class="result_wrap">
        <div class="result_content">
            <table class="list_tab">
                <tr>
                    <th class="tc">排序</th>
                    <th class="tc">ID</th>
                    <th>标题</th>
                    <th>名称</th>
                    <th>内容</th>
                    <th>操作</th>
                </tr>
            @foreach($data as $v)
                <tr>
                    <td class="tc">
                        <input type="text" onchange="changeOrder(this,{{$v['conf_id']}})" value="{{$v['conf_order']}}">
                    </td>
                    <td class="tc">{{$v['conf_id']}}</td>
                    <td>{{$v['conf_title']}}</td>
                    <td>{{$v['conf_name']}}</td>
                    <td>
                        {!! $v['html'] !!}
                    </td>
                    <td>
                        <a href="{{url('admin/config/'.$v['conf_id'].'/edit')}}">修改</a>
                        <a href="javascript:;" onclick="deleteConf({{$v['conf_id']}})">删除</a>
                    </td>
                </tr>
                @endforeach
            </table>
            </div>
        <div class="btn_group">
            <input type="submit" value="提交">
            <input type="button" class="back" onclick="history.go(-1)" value="返回" >
        </div>
        </div>
</form>
<script>
    function changeOrder(obj,conf_id){
        var conf_order=$(obj).val();
        $.post('{{url('admin/config/changeOrder')}}',{'_token':'{{csrf_token()}}','conf_id':conf_id,'conf_order':conf_order},function(data){
            if(data.status==1){
                layer.msg(data.msg,{icon:6});
            }else{
                layer.msg(data.msg,{icon:5});
            }
        });
    }

    function deleteConf(conf_id){
        layer.confirm('你确定要删除吗？',{
            btn:['确定','取消']
        },function(){
            $.post('{{url('admin/config')}}/'+conf_id,{'_method':'delete','_token':'{{csrf_token()}}'},function(data){
                if(data.status==1){
                    layer.msg(data.msg,{icon:6});
                    location.reload(true);
                }else{
                    layer.msg(data.msg,{icon:5});
                }
            });
        },function(){

        });
    }
</script>
    <!--搜索结果页面 列表 结束-->
@endsection