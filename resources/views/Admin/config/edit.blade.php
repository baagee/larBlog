@extends('admin.layouts')
@section('content')
    <!--面包屑导航 开始-->
    <div class="crumb_warp">
        <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
        <i class="fa fa-home"></i> <a href="{{url('admin/info')}}">首页</a> &raquo; 修改配置项
    </div>
    <!--面包屑导航 结束-->

	<!--结果集标题与导航组件 开始-->
	<div class="result_wrap">
        <div class="result_title">
            <h3>快捷操作</h3>
            {{--打印错误信息--}}
            @if(count($errors))
                <div class="mark">
                    @foreach($errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </div>
            @endif
        </div>
        <div class="result_content">
            <div class="short_wrap">
                <a href="{{url('admin/config')}}"><i class="fa fa-arrow-left"></i>返回配置项列表</a>
             {{--   <a href="#"><i class="fa fa-recycle"></i>批量删除</a>
                <a href="#"><i class="fa fa-refresh"></i>更新排序</a>--}}
            </div>
        </div>
    </div>
    <!--结果集标题与导航组件 结束-->
    
    <div class="result_wrap">
        <form action="{{url('admin/config/'.$conf['conf_id'])}}" method="post">
            {{csrf_field()}}
            <table class="add_tab">
                <tbody>
                    <tr>
                        <th><i class="require">*</i>名称：</th>
                        <td>
                            <input type="text" value="{{$conf['conf_name']}}" name="conf_name">
                        </td>
                    </tr>
                    <tr>
                        <th><i class="require">*</i>标题：</th>
                        <td>
                            <input type="text" value="{{$conf['conf_title']}}" class="lg" name="conf_title" >
                        </td>
                    </tr>
                    <tr>
                        <th><i class="require">*</i>类型：</th>
                        <td>
                            input：<input type="radio"
                                         @if($conf['field_type']=='input')
                                            checked=""
                                         @endif
                            onclick="showTR()" name="field_type" value="input"> &nbsp
                            radio：<input type="radio"
                                         @if($conf['field_type']=='radio')
                                            checked=""
                                         @endif
                            onclick="showTR()" name="field_type" value="radio"> &nbsp
                            textarea：<input type="radio"
                                            @if($conf['field_type']=='textarea')
                                                checked=""
                                            @endif
                            onclick="showTR()" name="field_type" value="textarea"> &nbsp
                        </td>
                    </tr>
                    <tr id="f_type">
                        <th><i class="require">*</i>类型值：</th>
                        <td>
                            <input type="text" class="md" name="field_value" value="{{$conf['field_value']}}">
                            <span><i class="fa fa-exclamation-circle yellow"></i>只有当类型为radio时才必须填（格式：1|开启,0|关闭）</span>
                        </td>
                    </tr>
                    <tr>
                        <th>排序：</th>
                        <td>
                            <input type="text" class="sm" name="conf_order" value="{{$conf['conf_order']}}">
                        </td>
                    </tr>
                    <tr>
                        <th><i class="require">*</i>说明：</th>
                        <td>
                        <textarea name="conf_tips" id="" cols="30" rows="10">{{$conf['conf_tips']}}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <input type="hidden" name="_method" value="put">
                            <input type="submit" value="提交">
                            <input type="button" class="back" onclick="history.go(-1)" value="返回">
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
<script>
    showTR();
    function showTR(){
        var type=$("input[name='field_type']:checked").val();
        if(type=='radio'){
            $("#f_type").show()
        }else{
            $("#f_type").hide()
        }
    }
</script>
@endsection