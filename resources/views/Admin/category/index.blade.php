@extends('admin.layouts')
        @section('content')
    <!--面包屑导航 开始-->
    <div class="crumb_warp">
        <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
        <i class="fa fa-home"></i> <a href="{{url('admin/info')}}">首页</a> &raquo; 全部分类
    </div>
    <!--面包屑导航 结束-->

    <!--搜索结果页面 列表 开始-->
    <form action="#" method="post">
        <div class="result_wrap">
            <div class="result_title">
                <h3>快捷操作</h3>
                </div>
            <!--快捷导航 开始-->
            <div class="result_content">
                <div class="short_wrap">
                    <a href="{{url('admin/category/create')}}"><i class="fa fa-plus"></i>新增分类</a>
                    {{--<a href="#"><i class="fa fa-recycle"></i>批量删除</a>--}}
                </div>
            </div>
            <!--快捷导航 结束-->
        </div>

        <div class="result_wrap">
            <div class="result_content">
                <table class="list_tab">
                    <tr>
                        <th class="tc">排序</th>
                        <th class="tc">ID</th>
                        <th>标题</th>
                        <th>描述</th>
                        <th>点击数</th>
                        <th>操作</th>
                    </tr>
                    @foreach($data as $v)
                    <tr>
                        <td class="tc">
                            <input type="text" onchange="changeOrder(this,{{$v['cat_id']}})" name="cat_order[]" value="{{$v['cat_order']}}">
                        </td>
                        <td class="tc">{{$v['cat_id']}}</td>
                        <td>
                            <a href="#">{{$v['cat_name']}}</a>
                        </td>
                        <td>{{$v['cat_title']}}</td>
                        <td>{{$v['cat_views']}}</td>
                        <td>
                            <a href="{{url('admin/category/'.$v['cat_id'].'/edit')}}">修改</a>
                            <a href="javascript:;" onclick="deleteCate({{$v['cat_id']}})">删除</a>
                        </td>
                    </tr>
                        @endforeach
                </table>
            </div>
        </div>
    </form>
    <!--搜索结果页面 列表 结束-->
<script>
    function changeOrder(obj,cat_id){
        var cat_order=$(obj).val();
        $.post('{{url('admin/category/changeOrder')}}',{'_token':'{{csrf_token()}}','cat_id':cat_id,'cat_order':cat_order},function(data){
            if(data.status==1){
                layer.msg(data.msg,{icon:6});
            }else{
                layer.msg(data.msg,{icon:5});
            }
        });
    }

    function deleteCate(cat_id){
        layer.confirm('你确定要删除吗？',{
            btn:['确定','取消']
        },function(){
            $.post('{{url('admin/category')}}/'+cat_id,{'_method':'delete','_token':'{{csrf_token()}}'},function(data){
                if(data.status==1){
                    layer.msg(data.msg,{icon:6});
                    location.reload(true);
                }else{
                    layer.msg(data.msg,{icon:5});
                }
            });
        },function(){

        });
    }
</script>
@endsection