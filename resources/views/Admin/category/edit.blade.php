@extends('admin.layouts')
@section('content')
    <!--面包屑导航 开始-->
    <div class="crumb_warp">
        <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
        <i class="fa fa-home"></i> <a href="{{url('admin/info')}}">首页</a> &raquo; 修改文章分类
    </div>
    <!--面包屑导航 结束-->

	<!--结果集标题与导航组件 开始-->
	<div class="result_wrap">
        <div class="result_title">
            <h3>快捷操作</h3>
            {{--打印错误信息--}}
            @if(count($errors))
                <div class="mark">
                    @foreach($errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </div>
            @endif
        </div>
        <div class="result_content">
            <div class="short_wrap">
                <a href="{{url('admin/category')}}"><i class="fa fa-arrow-left"></i>返回分类列表</a>
             {{--   <a href="#"><i class="fa fa-recycle"></i>批量删除</a>
                <a href="#"><i class="fa fa-refresh"></i>更新排序</a>--}}
            </div>
        </div>
    </div>
    <!--结果集标题与导航组件 结束-->
    
    <div class="result_wrap">
        <form action="{{url('admin/category/'.$oneCate['cat_id'])}}" method="post">
            {{csrf_field()}}
            <table class="add_tab">
                <tbody>
                    <tr>
                        <th width="120">父级分类：</th>
                        <td>
                            <select name="cat_pid">
                                <option value="0">==顶级分类==</option>
                                @foreach($topCate as $cate)
                                    <option @if($oneCate['cat_pid']==$cate['cat_id'])
                                            selected=""
                                            @endif
                                            value="{{$cate['cat_id']}}">{{$cate['cat_name']}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><i class="require">*</i>分类名称：</th>
                        <td>
                            <input type="text" value="{{$oneCate['cat_name']}}" name="cat_name">
                        </td>
                    </tr>
                    <tr>
                        <th><i class="require">*</i>标题：</th>
                        <td>
                            <input type="text" value="{{$oneCate['cat_title']}}" class="lg" name="cat_title" placeholder="标题可以写30个字">
                        </td>
                    </tr>
                    <tr>
                        <th>关键字：</th>
                        <td>
                            <input type="text" value="{{$oneCate['cat_keywords']}}" class="lg" name="cat_keywords">
                        </td>
                    </tr>
                    <tr>
                        <th>排序：</th>
                        <td>
                            <input type="text" value="{{$oneCate['cat_order']}}" class="sm" name="cat_order">
                        </td>
                    </tr>
                    <tr>
                        <th>简介：</th>
                        <td>
                            <textarea name="cat_description">{{$oneCate['cat_description']}}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <input type="hidden" name="_method" value="put">
                            <input type="submit" value="提交">
                            <input type="button" class="back" onclick="history.go(-1)" value="返回">
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
@endsection