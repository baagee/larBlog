@extends('admin.layouts')
@section('content')
    <!--面包屑导航 开始-->
    <div class="crumb_warp">
        <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
        <i class="fa fa-home"></i> <a href="{{url('admin/info')}}">首页</a>&raquo; 菜单链接列表
    </div>
    <!--面包屑导航 结束-->
    <!--搜索结果页面 列表 开始-->
        <div class="result_wrap">
            <div class="result_title">
                <h3>快捷操作</h3>
            </div>
            <!--快捷导航 开始-->
            <div class="result_content">
                <div class="short_wrap">
                    <a href="{{url('admin/navs/create')}}"><i class="fa fa-plus"></i>新增菜单链接</a>
                </div>
            </div>
            <!--快捷导航 结束-->
        </div>

        <div class="result_wrap">
            <div class="result_content">
                <table class="list_tab">
                    <tr>
                        <th class="tc">排序</th>
                        <th class="tc">ID</th>
                        <th>别名</th>
                        <th>名称</th>
                        <th>URL</th>
                        <th>操作</th>
                    </tr>
                @foreach($navs as $v)
                    <tr>
                        <td class="tc">
                            <input type="text" onchange="changeOrder(this,{{$v['nav_id']}})" name="nav_order" value="{{$v['nav_order']}}">
                        </td>
                        <td class="tc">{{$v['nav_id']}}</td>
                        <td>{{$v['nav_alias']}}</td>
                        <td>{{$v['nav_name']}}</td>
                        <td>
                            <a target="_blank" href="{{$v['nav_url']}}">{{$v['nav_url']}}</a>
                        </td>
                        <td>
                            <a href="{{url('admin/navs/'.$v['nav_id'].'/edit')}}">修改</a>
                            <a href="javascript:;" onclick="deleteLink({{$v['nav_id']}})">删除</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
                </div>
            </div>
<script>
    function changeOrder(obj,nav_id){
        var nav_order=$(obj).val();
        $.post('{{url('admin/navs/changeOrder')}}',{'_token':'{{csrf_token()}}','nav_id':nav_id,'nav_order':nav_order},function(data){
            if(data.status==1){
                layer.msg(data.msg,{icon:6});
            }else{
                layer.msg(data.msg,{icon:5});
            }
        });
    }

    function deleteLink(nav_id){
        layer.confirm('你确定要删除吗？',{
            btn:['确定','取消']
        },function(){
            $.post('{{url('admin/navs')}}/'+nav_id,{'_method':'delete','_token':'{{csrf_token()}}'},function(data){
                if(data.status==1){
                    layer.msg(data.msg,{icon:6});
                    location.reload(true);
                }else{
                    layer.msg(data.msg,{icon:5});
                }
            });
        },function(){

        });
    }
</script>
    <!--搜索结果页面 列表 结束-->
@endsection