<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="{{asset('/resources/views/Admin/style/css/ch-ui.admin.css')}}">
	<link rel="stylesheet" href="{{asset('/resources/views/Admin/style/font/css/font-awesome.min.css')}}">
</head>
<body style="background:#F3F3F4;">
	<div class="login_box">
		<h1>BaAGee</h1>
		<h2>欢迎使用博客管理平台</h2>
		<div class="form">
            @if(session('msg'))
			    <p style="color:red">{{session('msg')}}</p>
            @endif
			<form action="" method="post">
                {{--加入csrf验证--}}
                {{csrf_field()}}
				<ul>
					<li>
					<input type="text" name="username" class="text"/>
						<span><i class="fa fa-user"></i></span>
					</li>
					<li>
						<input type="password" name="password" class="text"/>
						<span><i class="fa fa-lock"></i></span>
					</li>
					<li>
						<input type="text" class="code" name="verify"/>
						<span><i class="fa fa-check-square-o"></i></span>
						<img src="{{url('/admin/verify')}}" onclick="this.src='{{'/admin/verify'}}?'+Math.random()" alt="验证码">
					</li>
					<li>
						<input type="submit"  value="立即登陆"/>
					</li>
				</ul>
			</form>
			<p> &copy; 2016 Powered by <a href="http://www.baagee.top" target="_blank">http://www.baagee.top</a></p>
		</div>
	</div>
</body>
</html>