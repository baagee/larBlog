@extends('admin.layouts')
@section('content')
    <script type="text/javascript" charset="utf-8" src="{{asset('resources/extends/ueditor/ueditor.config.js')}}"></script>
    <script type="text/javascript" charset="utf-8" src="{{asset('resources/extends/ueditor/ueditor.all.min.js')}}"> </script>
    <script type="text/javascript" charset="utf-8" src="{{asset('resources/extends/ueditor/lang/zh-cn/zh-cn.js')}}"></script>
    <script type="text/javascript" src="{{asset('resources/extends/uploadify/jquery.uploadify.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('resources/extends/uploadify/uploadify.css')}}">
    <style>
        .edui-default{line-height: 28px;}
        div.edui-combox-body,div.edui-button-body,div.edui-splitbutton-body
        {overflow: hidden; height:20px;}
        div.edui-box{overflow: hidden; height:22px;}
    </style>
    <style>
        .uploadify{display:inline-block;}
        .uploadify-button{border:none; border-radius:5px; margin-top:8px;}
        table.add_tab tr td span.uploadify-button-text{color: #FFF; margin:0;}
    </style>
    <!--面包屑导航 开始-->
    <div class="crumb_warp">
        <i class="fa fa-home"></i> <a href="{{url('admin/info')}}">首页</a> &raquo; 添加文章
    </div>
    <!--面包屑导航 结束-->

	<!--结果集标题与导航组件 开始-->
	<div class="result_wrap">
        <div class="result_title">
            <h3>快捷操作</h3>
            @if(count($errors))
                <div class="mark">
                    @foreach($errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </div>
            @endif
        </div>
        <div class="result_content">
            <div class="short_wrap">
                <a href="{{url('admin/article')}}"><i class="fa fa-arrow-left"></i>文章列表</a>
            </div>
        </div>
    </div>
    <!--结果集标题与导航组件 结束-->
    
    <div class="result_wrap">
        <form action="{{url('admin/article')}}" method="post">
            {{csrf_field()}}
            <table class="add_tab">
                <tbody>
                    <tr>
                        <th><i class="require">*</i>标题：</th>
                        <td>
                            <input type="text" class="lg" name="art_title">
                        </td>
                    </tr>
                    <tr>
                        <th width="120">文章分类：</th>
                        <td>
                            <select name="art_cat_id">
                                <option value="">==选择文章分类==</option>
                                @foreach($cates as $cate)
                                    <option value="{{$cate['cat_id']}}">{{$cate['cat_name']}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><i class="require">*</i>标签：</th>
                        <td>
                            <input type="text" class="lg" name="art_tag">
                        </td>
                    </tr>
                    <tr>
                        <th><i class="require">*</i>作者：</th>
                        <td>
                            <input type="text" name="art_editor">
                        </td>
                    </tr>
                    <tr>
                        <th rolspan="2"><i class="require">*</i>缩略图：</th>
                        <td> <input type="text" class="md" id="art_thumb" readonly value="" name="art_thumb"><input id="file_upload" name="file_upload" type="file" multiple="true"></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><img id="showimg" src="" style="max-width: 100px;max-height:100px" alt="缩略图"></td>
                    </tr>
                    <tr>
                        <th>描述：</th>
                        <td>
                            <textarea name="art_description"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>文章内容：</th>
                        <td>
                            <script id="editor" name="art_content" type="text/plain" style="width:95%;height:400px;"></script>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <input type="submit" value="提交">
                            <input type="button" class="back" onclick="history.go(-1)" value="返回">
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <script>
        var ue = UE.getEditor('editor');
    </script>
    <script type="text/javascript">
        $(function() {
            $('#file_upload').uploadify({
                'buttonText':'选择图片',
                'formData'     : {
                    '_token'     : "{{csrf_token()}}"
                },
                'swf'      : "{{asset('resources/extends/uploadify/uploadify.swf')}}",
                'uploader' : "{{asset('admin/upload')}}",
                'onUploadSuccess': function(file,data,response){
                    $("#art_thumb").val(data);
                    $("#showimg").attr('src','/'+data);
                }
            });
        });
    </script>
@endsection