@extends('admin.layouts')
@section('content')
    <style>
        .result_content ul li span {
            padding: 6px 12px;
    </style>
    <!--面包屑导航 开始-->
    <div class="crumb_warp">
        <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
        <i class="fa fa-home"></i> <a href="{{url('admin/info')}}">首页</a> &raquo; 文章列表
    </div>
    <!--面包屑导航 结束-->

	<!--结果页快捷搜索框 开始-->
	<div class="search_wrap">
        <form action="">
            <table class="search_tab">
                <tr>
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="get">
                    <th width="120">选择分类:</th>
                    <td>
                        <select name="art_cat_id">
                            <option value="">==选择文章分类==</option>
                            @foreach($cates as $cate)
                                <option
                                        @if($cat_id==$cate['cat_id'])
                                                selected=""
                                                @endif
                                        value="{{$cate['cat_id']}}">{{$cate['cat_name']}}</option>
                            @endforeach
                        </select>
                    </td>
                    <th width="70">关键字:</th>
                    <td><input type="text" value="{{$tag}}" name="art_tag" placeholder="关键字"></td>
                    <th width="70">标题:</th>
                    <td><input type="text" value="{{$title}}" name="art_title" placeholder="标题"></td>
                    <td><input type="submit" value="查询"></td>
                </tr>
            </table>
        </form>
    </div>
    <!--结果页快捷搜索框 结束-->

    <!--搜索结果页面 列表 开始-->
    <form action="#" method="post">
        <div class="result_wrap">
            <div class="result_title">
                <h3>快捷操作</h3>
            </div>
            <!--快捷导航 开始-->
            <div class="result_content">
                <div class="short_wrap">
                    <a href="{{url('admin/article/create')}}"><i class="fa fa-plus"></i>新增文章</a>
                </div>
            </div>
            <!--快捷导航 结束-->
        </div>

        <div class="result_wrap">
            <div class="result_content">
                <table class="list_tab">
                    <tr>
                        <th class="tc">ID</th>
                        <th>标题</th>
                        <th>缩略图</th>
                        <th>关键字</th>
                        <th>分类</th>
                        <th>点击</th>
                        <th>发布人</th>
                        <th>发布时间</th>
                        <th>操作</th>
                    </tr>
                    @foreach($data as $v)
                    <tr>
                        <td class="tc">{{$v['art_id']}}</td>
                        <td>
                            <a href="#">{{$v['art_title']}}</a>
                        </td>
                        <td>
                            @if($v['art_thumb']!='')
                            <img style="max-height: 40px;" src="/{{$v['art_thumb']}}" alt="缩略图">
                                @else
                                没图 :(
                            @endif
                        </td>
                        <td>{{$v['art_tag']}}</td>
                        <td>{{$v['cat_name']}}</td>
                        <td>{{$v['art_views']}}</td>
                        <td>{{$v['art_editor']}}</td>
                        <td>{{date('Y-m-d H:i:s',$v['art_time'])}}</td>
                        <td>
                            <a href="{{url('admin/article/'.$v['art_id'].'/edit')}}">修改</a>
                            <a href="javascript:;" onclick="deleteArt({{$v['art_id']}})">删除</a>
                        </td>
                    </tr>
                    @endforeach
                </table>

                <div class="page_list">
                    {{$data->links()}}
                </div>
            </div>
        </div>
    </form>
    <!--搜索结果页面 列表 结束-->
    <script>
        function deleteArt(art_id){
            layer.confirm('你确定要删除吗？',{
                btn:['确定','取消']
            },function(){
                $.post('{{url('admin/article')}}/'+art_id,{'_method':'delete','_token':'{{csrf_token()}}'},function(data){
                    if(data.status==1){
                        layer.msg(data.msg,{icon:6});
                        location.reload(true);
                    }else{
                        layer.msg(data.msg,{icon:5});
                    }
                });
            },function(){

            });
        }
    </script>
@endsection