@extends('admin.layouts')
@section('content')
    <!--面包屑导航 开始-->
    <div class="crumb_warp">
        <!--<i class="fa fa-bell"></i> 欢迎使用登陆网站后台，建站的首选工具。-->
        <i class="fa fa-home"></i> <a href="{{url('admin/info')}}">首页</a>&raquo; 友情链接列表
    </div>
    <!--面包屑导航 结束-->
    <!--搜索结果页面 列表 开始-->
        <div class="result_wrap">
            <div class="result_title">
                <h3>快捷操作</h3>
            </div>
            <!--快捷导航 开始-->
            <div class="result_content">
                <div class="short_wrap">
                    <a href="{{url('admin/links/create')}}"><i class="fa fa-plus"></i>新增友情链接</a>
                </div>
            </div>
            <!--快捷导航 结束-->
        </div>

        <div class="result_wrap">
            <div class="result_content">
                <table class="list_tab">
                    <tr>
                        <th class="tc">排序</th>
                        <th class="tc">ID</th>
                        <th>标题</th>
                        <th>名称</th>
                        <th>URL</th>
                        <th>操作</th>
                    </tr>
                @foreach($links as $v)
                    <tr>
                        <td class="tc">
                            <input type="text" onchange="changeOrder(this,{{$v['link_id']}})" name="link_order" value="{{$v['link_order']}}">
                        </td>
                        <td class="tc">{{$v['link_id']}}</td>
                        <td>{{$v['link_title']}}</td>
                        <td>{{$v['link_name']}}</td>
                        <td>
                            <a target="_blank" href="{{$v['link_url']}}">{{$v['link_url']}}</a>
                        </td>
                        <td>
                            <a href="{{url('admin/links/'.$v['link_id'].'/edit')}}">修改</a>
                            <a href="javascript:;" onclick="deleteLink({{$v['link_id']}})">删除</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
                </div>
            </div>
<script>
    function changeOrder(obj,link_id){
        var link_order=$(obj).val();
        $.post('{{url('admin/links/changeOrder')}}',{'_token':'{{csrf_token()}}','link_id':link_id,'link_order':link_order},function(data){
            if(data.status==1){
                layer.msg(data.msg,{icon:6});
            }else{
                layer.msg(data.msg,{icon:5});
            }
        });
    }

    function deleteLink(link_id){
        layer.confirm('你确定要删除吗？',{
            btn:['确定','取消']
        },function(){
            $.post('{{url('admin/links')}}/'+link_id,{'_method':'delete','_token':'{{csrf_token()}}'},function(data){
                if(data.status==1){
                    layer.msg(data.msg,{icon:6});
                    location.reload(true);
                }else{
                    layer.msg(data.msg,{icon:5});
                }
            });
        },function(){

        });
    }
</script>
    <!--搜索结果页面 列表 结束-->
@endsection