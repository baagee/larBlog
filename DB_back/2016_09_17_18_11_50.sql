-- MySQL dump 10.13  Distrib 5.5.38, for Win32 (x86)
--
-- Host: localhost    Database: larblog
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `baagee_article`
--

DROP TABLE IF EXISTS `baagee_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baagee_article` (
  `art_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `art_title` varchar(100) NOT NULL DEFAULT '' COMMENT '文章标题',
  `art_tag` varchar(95) NOT NULL DEFAULT '' COMMENT '标签',
  `art_description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `art_thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '缩略图',
  `art_content` text NOT NULL COMMENT '文章主体',
  `art_time` int(12) DEFAULT '0' COMMENT '创建时间',
  `art_editor` varchar(55) NOT NULL DEFAULT '' COMMENT '作者',
  `art_views` varchar(255) NOT NULL DEFAULT '0' COMMENT '查看次数',
  `art_cat_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '分类ID',
  PRIMARY KEY (`art_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='文章表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baagee_article`
--

LOCK TABLES `baagee_article` WRITE;
/*!40000 ALTER TABLE `baagee_article` DISABLE KEYS */;
INSERT INTO `baagee_article` VALUES (17,'测试测试测试','测试','测试测试测试测试测试测试测试测试测试测试测试测试','uploads/thumbImg/2016_09_17_00_04_44587.jpg','<p>测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试</p>',1474041893,'baagee','0',1),(18,'CHEHIS2','CHEHIS2','CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2','uploads/thumbImg/2016_09_17_00_50_41541.jpg','<p>CHE<span style=\"text-decoration: underline;\">HIS2CHEHIS2C</span>HEHI<span style=\"border: 1px solid rgb(0, 0, 0);\"><em>S2CHEHIS2CHEHIS2CHEHIS2CH</em>EHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2<strong>CHEHIS2CHEHIS2CHEHIS</strong></span><strong>2CH</strong><span style=\"color: rgb(255, 0, 0);\"><strong>EHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CHEHIS2CH</strong>EHIS2</span>CHEHIS2</p>',1474044671,'CHEHIS2','0',4),(20,'潍坊学院网络信息中心','通知公告中心 ','近期，网络信息中心将对学生公寓网络进行升级改造，包括：\r\n学生公寓有线网络线路的升级、改造；\r\n全校范围内无线网络的建设；\r\n计费系统、认证方式和管理办法；','uploads/thumbImg/2016_09_17_11_20_14675.jpg','<h2 style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px 0px 15px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 31.5px;\">近期，网络信息中心将对学生公寓网络进行升级改造，包括：</h2><h2 style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px 0px 15px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 31.5px;\">学生公寓有线网络线路的升级、改造；</h2><h2 style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px 0px 15px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 31.5px;\">全校范围内无线网络的建设；</h2><h2 style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px 0px 15px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 31.5px;\">计费系统、认证方式和管理办法；</h2><h2 style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px 0px 15px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 31.5px;\"><span style=\"text-decoration: underline;\">调整以后，原有计费系统的余额会自动迁移至新系统。</span></h2><h2 style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px 0px 15px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 31.5px;\"><span style=\"text-decoration: underline;\">为防止出现不必要的问题，请同学们不要一次性充值过多。</span></h2><h2 style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px 0px 15px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 31.5px;\">在此期间，潍院网管会将暂停公寓网络线路及信息插座的更换。无需更换的一般网络故障，如校园网客户端、网络账户等问题，潍院网管会继续提供技术支持和维修维护。</h2><h2 style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px 0px 15px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 31.5px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 网络信息中心</h2><h2 style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px 0px 15px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 31.5px;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2016.8.28</h2><h3 style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px 0px 10px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 24.5px; padding: 0px;\">About<span class=\"Apple-converted-space\">&nbsp;</span><span rel=\"author\"><a href=\"http://it.wfu.edu.cn/?author=1\" title=\"由wfud发布\" rel=\"author\" style=\"text-decoration: underline; color: rgb(27, 134, 210);\">Wfud</a></span></h3><h3 id=\"comments-title\" style=\"break-after: avoid; orphans: 3; widows: 3; margin: 0px; font-family: inherit; font-weight: 300; line-height: 40px; color: inherit; text-rendering: optimizeLegibility; font-size: 24.5px; padding: 30px 0px 0px; text-align: center !important;\">8 Responses to 通知</h3><p><br/></p>',1474082437,'潍坊学院网络信息中心','0',4),(19,'CHEHIS3','CHEHIS3','CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3','uploads/thumbImg/2016_09_17_00_51_43258.jpg','<p>CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3CHEHIS3</p>',1474044709,'CHEHIS3','0',7),(21,'上网打开网页提示502 bad gateway是什么意思？如何解决？','上网   502   Bad   Gateway   ','这篇文章主要介绍了上网打开网页提示502 bad gateway是什么意思？如何解决？本文先是讲解了502 bad gateway是什么意思,然后给出了4种解决方法,需要的朋友可以参考下','uploads/thumbImg/2016_09_17_11_32_36249.png','<p>上网的时候经常会遇到这样那样的代码 比如502 bad gateway是就常见的错误代码，502 bad gateway是什么意思呢？如何解决呢？</p><p><strong>502 bad gateway是什么意思</strong></p><p>错误的网关</p><p>通俗的来讲，就是Web 服务器作为网关或代理服务器时收到无效的响应。<br/>&nbsp;<br/>&nbsp;<br/><strong>502 bad gateway解决办法</strong></p><p>主要还是看一下别人是不是一样不能访问，如果都不能访问基本上是服务器挂了。别人可以访问，本机再ctrl+F5强制刷新一次试试，如果还不行，再看如下情况</p><p><strong>第一&nbsp; DNS 缓冲问题</strong></p><p>这种情况主要是你在没有用vpn的情况下,访问了国外一些被国家防火墙屏蔽的网站如YouTube,facebook之类。<br/>&nbsp;<br/>解决办法： 这种情况通常在几分钟之内就可以访问了。也可以尝试 在cmd窗口运行 ipconfig /flushdns 来刷新DNS缓存<br/>&nbsp;<br/><strong>第二&nbsp; 浏览器开了代理，而且代理无用。</strong></p><p>解决办法：关掉代理。<br/>&nbsp;<br/><strong>第三 dns 被劫持</strong></p><p>解决办法：更换DNS，推荐 114.114.114.114&nbsp;&nbsp; 还有阿里的公共DNS 如：223.5.5.5&nbsp;&nbsp;&nbsp; 223.6.6.6<br/>&nbsp;<br/><strong>最后再提供一个特殊情况</strong></p><p>软件劫持&nbsp; 天启日历<br/>把相关程序结束就行。比如<br/>用360加速-----网速-----详情-----找到“天启日历”-----右边“管理&quot; ----结结束进程</p><p><br/></p>',1474083197,'baagee','42',5),(22,'Uber在美首推无人驾驶载客 23%美国人不敢坐','Uber无人驾驶','Uber无人驾驶汽车 据新华社电 美国打车软件公司优步14日推出无人驾驶汽车载客服务，在美国东部宾夕法尼亚州匹兹堡市上路试运行。当天，4辆车型为福特Fusion的无人驾驶汽车“亮相”街头 ...','uploads/thumbImg/2016_09_17_14_22_25116.jpg','<p>据新华社电 美国打车软件公司优步14日推出无人驾驶汽车载客服务，在美国东部宾夕法尼亚州匹兹堡市上路试运行。</p><p>　　当天，4辆车型为福特Fusion的无人驾驶汽车“亮相”街头。它们配有多个摄像头、全球定位系统以及用以探测天气和道路状况的雷达系统和其他传感器等，在匹兹堡复杂多变的道路上行驶，搭载着优步软件的常客。</p><p>　　<strong>“能帮助缓解交通拥堵”</strong></p><p>　　美联社报道，试运行当天，4辆无人驾驶汽车按照正常程序“接单”，约到无人驾驶汽车的乘客，将免费体验无人驾驶汽车载客服务。</p><p>　　尽管依靠汽车自动驾驶，但为保险起见，每辆无人驾驶汽车上配有两名工程师，以确保车辆运行无碍。他们其中一人坐在驾驶座上，准备在棘手的路段随时控制车辆，另外一人则监控汽车的动态。</p><p>　　27岁的泰勒·波利耶是当天约到无人驾驶汽车的乘客之一。他说，在去往其工作地点的15分钟车程中，这辆福特Fusion“感觉灵敏”，一路顺利，和“平常乘坐优步的载客汽车一样”。</p><p>　　“试运行又让我们前进了一步，”设在匹兹堡的优步先进技术中心主管拉菲·克里科里安说，“我们认为，这（无人驾驶汽车）能够帮助缓解交通拥堵，让交通变得更加便宜、便利。”</p><p>　　按路透社的说法，匹兹堡道路较为复杂，到处是狭窄和陡峭的街道，还有不少隧道和超过40座桥梁。</p><p>　　“事实上，我们认为匹兹堡是高难度驾驶地区，”克里科里安说，“如果我们（的无人驾驶汽车）能够应对匹兹堡的路段，那世界上其他大部分城市的路况就更容易应对了。”</p><p>　　<strong>逐步推进 最终“纯无人驾驶”</strong></p><p>　　美联社报道，在试运行载客前，优步无人驾驶汽车在匹兹堡上路测试的时间不到两年。但测试结果表明，这些车辆足以应对多数情况，与不少真人驾驶车辆无异。</p><p>　　今年春季，优步员工首先乘坐这些无人驾驶汽车上下班，为载客服务的推出作准备。</p><p>　　14日的试运行也使优步成为美国首个推出无人驾驶汽车载客的公司。</p><p>　　该公司表示，除了4辆试运行车辆，优步至少还有另外12辆无人驾驶汽车做好上路载客准备。优步计划，逐步将随车的两名工程师减少至一名，最终实现“纯无人驾驶”。</p><p><ins style=\"display: block; overflow: hidden; text-decoration: none;\" data-ad-status=\"done\" id=\"SinaAdsArtical\" class=\"sinaads sinaads-done\" data-ad-pdps=\"PDPS000000056054\"><ins style=\"text-decoration:none;margin:0px auto;width:300px;display:block;position:relative;\"></ins></ins></p><p>　　“我们拥有全球最强大的无人驾驶工程团队之一，以及在数百座城市经营拼车服务的丰富经验，”优步创始人兼CEO特拉维斯·卡拉尼克说。</p><p>　　<strong>无人驾驶的士真正载客 还需几年</strong></p><p>　　尽管已推出试运行，一些专家预测，优步“纯无人驾驶汽车”载客的计划需要几年甚至几十年才能实现。</p><p>　　“如果无人驾驶汽车在公路上以大约70英里（112公里）的时速运行时，一旦出现故障，情况会变得非常糟糕，”美国卡内基－梅隆大学教授拉杰·拉杰库马尔说，“这项技术必须相当可靠才能让随车人员彻底离开汽车。”</p><p>　　而能否让乘客安心选乘无人驾驶汽车也是一大挑战。</p><p>　　美国密歇根大学14日公布的一项研究结果表明，约23%的美国人明确表示自己不会乘坐无人驾驶汽车。</p><p>　　“我相当怀疑，这些无人驾驶汽车能否应对冰雪覆盖的冬季，”匹兹堡市民罗伯特·阿米蒂奇说。</p><p>　　对此，优步官员表示，希望通过首次试运行，缓解公众对无人驾驶汽车技术的恐惧。新华</p><p><br/></p>',1474093361,'baagee','0',4),(23,'滴滴的后独角兽时代 能否保持初心？ ','滴滴分享经济Uber','周六晚10点，北京朝阳大悦城结束了一天的喧嚣，繁华落幕。而商场外的马路上，忙碌继续上演：成群的私家车紧挨路边打开双闪，成群的人拨着电话在路口东张西望大声地喊叫着，希望在密密麻麻的车灯里寻找到自己通过手机订的专车。','uploads/thumbImg/2016_09_17_14_42_22850.gif','<p>周六晚10点，北京朝阳大悦城结束了一天的喧嚣，繁华落幕。而商场外的马路上，忙碌继续上演：成群的私家车紧挨路边打开双闪，成群的人拨着电话在路口东张西望大声地喊叫着，希望在密密麻麻的车灯里寻找到自己通过手机订的专车。</p><p>　　从某种意义上而言，打车软件已经成为人们出行中不可或缺的一部分。而滴滴出行——在完成对Uber中国的收购后，已经成为这一领域绝对意义上的独角兽。</p><p>　　滴滴变革、发展的速度远超很多公司，CEO程维曾表示“可以说滴滴每三个月便是一家新的公司”。8月1日至今，在收购中国优步的一个月时间里，从整合到调价再到开展新业务，滴滴进行了一系列的调整，试图加速完成从竞争者到守局人的角色转变。</p><p>　　<strong>优滴出现了吗？</strong></p><p>　　滴滴与Uber中国合并当晚，Uber中国召开全体员工会议，表示将在合并后30天内给员工发放合并完成现金奖励（Close \r\nBonus），届时员工可以选择留下或者离开。宣布合并一星期后，程维给中国优步的员工发了一封名为“One Team，One \r\nDream”的内部邮件，希望中国优步独立运营。在“七夕”情人节前夕，这封邮件可被视为“示好的情书”，但此后从外部很难看到更多合并的实质性举措。</p><p>　　早在收购宣布之初，就有彭博社援引知情人士消息称，Uber把中国业务卖给滴滴后，将重新部署中国区的150名工程师，他们将被分配到新加坡、\r\n泰国、印尼等地区，继续地图开发等业务。近日有内部员工透露，滴滴已经成立了滴滴优步事业部，由滴滴和Uber中国双方各派两人，未来共同管理Uber中\r\n国的应用软件运营。</p><p>　　合并后的30天过渡即将结束，Uber中国员工将面临去留的选择，据此前一位管理层向界面透露，合并一宣布他就开始考虑找下家了，而与滴滴相关业务重合度很高的一些职位，Uber中国区员工也更倾向于另谋出路。</p><p>　　企业文化理念融合一向是企业并购中极具挑战的部分。此前，滴滴收购快的进展顺利，但Uber毕竟是管理理念、企业文化差异很大的跨国公司，合并是为了快速停止“烧钱大战”的市场策略，未来是否能发挥出1+1&gt;2的真正价值，还有待考验。</p><p>　　<strong>快速止损 司机离场</strong></p><p>　　相对内部整合而言，外部立刻注意到合并后的改变，就是滴滴针对乘客端的补贴减少、价格提高、对司机端的计价方式展开调整。</p><p>　　8月18日起，滴滴平台开始实行司机和乘客分开计价，取消之前对司机端收取的每单20%抽成，每单收取0.5元，外加1.77%管理费。滴滴方\r\n面表示，部分城市车主的收入还会随着司乘计价的分离而有所提高，大约可提高10%-18%。但车主们似乎并不在意，在谈到滴滴调整计价方式时，车主李先生\r\n面无表情地说：“滴滴会让我们赚钱？都一样的，背着抱着一般沉。”</p><p>　　8月3日，多名北京滴滴快车司机向<a class=\"wt_article_link\" href=\"http://weibo.com/wowjiemian?zw=tech\" target=\"_blank\">界面新闻</a>记者证实，滴滴取消了当天司机端的成单奖，而之后的几天里，成单奖回归：22单60元。现在，成单奖近一步降低，变成了22单40元。“以后应该会逐步取消奖励吧。平台司机那么多，你不干肯定有人愿意干。”李先生无奈地表示。</p><p>　　补贴下滑，订单减少，部分租车跑滴滴的车主、直营车主以及兼职车主有了逃离的打算。</p><p>　　滴滴司机王师傅之前在圆通快递做货车司机，觉得太累转行当滴滴司机开小车。不过仅三个月的时间，高额的租车费用让他打了退堂鼓：车辆日租每天\r\n200多元租金，一个月6000元。“平台还要抽成，算下来一个月有五六千的收入，但每天十几个小时的工作量。”9月份王师傅有了新计划，“和之前的公司\r\n联系好了，我还是回圆通开我的大货车了。”</p><p>　　开着红色的马自达，左臂纹身，戴着耳钉，酷酷的车主刘先生对界面新闻记者抱怨道：“滴滴坑苦我了！”他去年在一家公司当职员，每个月拿6000\r\n元薪水，当听说拉滴滴赚钱时，毅然辞去了稳定的工作，成为一名快车车主。去年下半年时他的收益确实不错，每个月都有1万多元的收入。“现在刨去油钱、保险\r\n钱和杂七杂八费用，一个月也就赚5000元钱。”刘先生也开始找工作，随时准备抽身。</p><p>　　不仅是在北京，深圳的滴滴车主也有这样的想法。周五下午，几个广西老乡一起来到滴滴车主之家，准备办理退出平台手续。一位车主表示，以前只是花\r\n业余时间开滴滴，之后花钱买了一辆轩逸专职跑，为了还车贷每天要开12个小时以上的车，以前觉得滴滴好是因为不像出租车那样被束缚。现在，订单数量下降很\r\n快，每天工作12-14个小时，每月才能赚6000多元钱，压力非常大。</p><p>　　而不少兼职车主则处于观望状态。“我白天上班，晚上拉滴滴出来兜兜风，赚个零花钱。”车主王先生说，“听说11月新政出台，还不知道是怎么样的呢，要是太严还要考证什么的，我就不干了。”</p><p>　　之前为了获得中国市场份额的“烧钱大战”，因为不菲的补贴迅速催生了一批“全职”司机，而随着收入的回落，势必会有人退出。抱着“上班之外赚零花钱”心态做兼职车主，似乎让滴滴平台更回归到理性和分享经济的初衷。</p><p>　　不过，优惠减少，滴滴也面临着与乘客的博弈。不少乘客都会在社交平台上感慨，滴滴打车涨价了。据《华商报》报道，自Uber和滴滴宣告合并后，\r\n全国市场涨价三成左右。其援引业内分析认为，消费者感觉到涨价，主要因为“网约车在提价的同时，各种补贴优惠也在减少，涨价是二者合力的结果”。</p><p>　　滴滴快车比出租车便宜的价格，仍会吸引人们作为出行选择。但这同样也是对于价格敏感的消费群体，势必会因为价格上涨而减少使用次数。从8月开始，北京有滴滴司机向界面新闻记者反映一天平均比以前减少了四五单。</p><p>　　《21世纪经济报道》援引易观智库数据显示，随着乘客优惠的逐步减少，相比5月，滴滴6月的用户渗透率开始小幅度下降。到了7月，滴滴的用户渗透率出现了较大的下滑，跌幅近10%。此外，滴滴在7月的活跃用户数也降低了超70万，其用户粘性也出现了连续的下降。</p><p>　　提升价格，说明滴滴需要对此前的“烧钱大战”快速止损，滴滴收购Uber后，滴滴投资人朱啸虎在接受采访时表示，“他们不能这样打下去，应该在达到300亿美元之前停火。”而现在，这只合并后的独角兽承载更多的是来自投资人的盈利压力。</p><p>　　根据滴滴此前公告，原Uber中国的中国股东将成为滴滴出行的投资人，获得滴滴方面合计2.3%的股份。而原Uber中国的中资股东包括中信证券、<span id=\"usstock_LFC\"><a href=\"http://stock.finance.sina.com.cn/usstock/quotes/LFC.html\" class=\"keyword f_st\" target=\"_blank\">中国人寿</a></span><span id=\"quote_LFC\"></span>、中国太平、海航集团、广汽集团及万科等。滴滴也因此成为了唯一一家<span id=\"hkstock_hk00700\"><a href=\"http://stock.finance.sina.com.cn/hkstock/quotes/00700.html\" class=\"keyword f_st\" target=\"_blank\">腾讯</a></span><span id=\"quote_hk00700\"></span>、<span id=\"usstock_BABA\"><a href=\"http://stock.finance.sina.com.cn/usstock/quotes/BABA.html\" class=\"keyword f_st\" target=\"_blank\">阿里巴巴</a></span><span id=\"quote_BABA\"></span>和<span id=\"usstock_BIDU\"><a href=\"http://stock.finance.sina.com.cn/usstock/quotes/BIDU.html\" class=\"keyword f_st\" target=\"_blank\">百度</a></span><span id=\"quote_BIDU\"></span>共同投资的企业。</p><p>　　而对于滴滴平台开始实行司机和乘客分开计价，有业内专家认为此举是滴滴在现有《垄断法》下，获得政府批准收购优步的策略之一。按着原有收费模\r\n式，滴滴营业收入为20%的平台服务费，网约车平台服务作为相关市场，收购Uber中国后就占到了整体市场份额80％以上；而采用司机乘客分别计价，乘客\r\n支付给滴滴的出行服务费作为营业收入，而司机收入记入滴滴营业成本，那么滴滴和优步中国占出行市场的份额距《垄断法》中“经营者集中”的标准还相差较远。</p><p>　　不过，也有观点认为，这种分开计价形式也改变了滴滴“分享经济”的商业模式。此前收取20%服务费的模式，意味着滴滴作为出行平台并不直接提供\r\n出行服务，而是为交易双方提供相关服务从中收取相应的服务费。而计价分离后，滴滴通过乘客和司机之间的价格差获得收益，这意味着滴滴从原来的分享经济平台\r\n角色变为了出行服务的转包商。</p><p>　　<strong>独角兽仍有对手</strong></p><p>　　虽然成为后Uber时代的独角兽，但滴滴一刻也不敢放松，在中国风起云涌的专车市场，其仍然面临着诸多有形和无形的挑战。</p><p>　　最直接的竞争来自其他平台。滴滴、Uber停止了“烧钱大战”，专车市场上的其他玩家持续补贴，易到方面继续推出150%的充返力度，并在此基础上赠送乐视会员与硬件等产品；神州方面，继续充100送20的活动；首汽约车延续充100送50的优惠力度。</p><p>　　据腾讯科技，路透社所调查的11位经济学家认为，进入打车应用市场几乎没有多少壁垒，因此这将是一个充满永恒竞争的行业。</p><p>　　主要依靠合约司机和几秒钟就能下载完毕的免费打车应用，平台司机和用户的忠诚度通常也不会很高。全球经济集团（Global \r\nEconomics \r\nGroup）董事长大卫·埃文斯说：“如果你的好友不在上面，你可能不会尝试使用一个新的社交网站，但是你不会在意你的好友使用哪款打车应用。”</p><p>　　专车新政在地方落地的不确定性亦成为滴滴不确定的市场因素。7月28日，专车新政终于出台。根据规定，新政将于11月起实施，网约车合法地位获得明确，满足条件的私家车可按程序转为网约车。但对于车辆标准、经营范围和指导价格等都给予了地方充分的自主权。</p><p>　　8月28日，《齐鲁晚报》援引内部人士消息，经过一个月的调研，济南版网约车草案已制定，网约车数量和运价都将受管控。此前济南运管中心和济南\r\n市出租汽车协会向出租车公司下发了《深化出租汽车改革热点问题11问答》，文中表示：“网约车车辆档次应明显高于当地主流巡游出租汽车，为市民提供高品质\r\n差异化出行服务。”</p><p>　　兰州市城运处相关负责人也表示将严格控制网约车的发展规模。根据市场需求合理制定网约车投放数量，在准入端对网约车实行合理管控。据了解，兰州\r\n市运营车辆饱和状态应该在1.5万辆左右，而目前兰州市出租车的保有量为1万辆，加上政府未来两年在出租车方面的投入，应该达到1.2万辆。照此计算，留\r\n给网约车的数量仅为3000辆左右。</p><p>　　除了需要继续稳固打车平台，滴滴也正借助其平台流量优势推进新业务的开发，试图建立一个完整的汽车生态版图。8月22日，滴滴出行确认旗下租车\r\n业务已于今年7月开始在上海测试运营，年内将推广到全国多个一二线城市。与传统租车不同，滴滴采取全程线上化租车服务，租车预订、支付及订单修改等环节均\r\n可以在线完成，工作人员免费送车上门。</p><p>　　此举被认为是切入了神州专车的后院——神州租车。对此，神州优车董事长陆正耀对媒体表示；“我们已经做好了开始打价格战的准备。但是打这个仗我们也不怕，第一就是我们的成本优势，另外我们的客户体验你是不能替代的。”另外陆正耀认为纯做租车平台的商业模式不成立。</p><p>　　从用滴滴打出租，再到用滴滴快车、专车、顺风车、租车……这个成立仅四年的互联网公司用巨额补贴“教育”了用户，滴滴作为中国出行市场的独角兽，在某种程度上也已经成为了整个社会出行的“必需品”。</p><p>　　短短一个月的时间里，网约车新政，巨头合并，戛然而止的补贴大战，滴滴突然进入了后独角兽时代。程维曾说过滴滴还只是搭建了出行生态的雏形，一切才刚刚开始，如何去填满“富有美好的想象空间”而不忘初心，人们都在盯着滴滴的下一步。</p><p><br/></p>',1474094577,'baagee','3',4),(24,'切尔西头号暗雷遭引爆！3500万买来解药or毒药? ','切尔西利物浦孔蒂路易斯','新浪体育讯　　孔蒂入主切尔西后，蓝军在8月的3场英超联赛中取得了全胜的成绩。然而，进入9月，意大利人突然发现在英超中并不那么容易拿分了。在上周末客场战平斯旺西之后，今天切尔西又在主场输给了利物浦。[小炮准确命中利物浦客胜][查看今日众豪门推荐]','uploads/thumbImg/2016_09_17_16_43_43762.jpg','<p>新浪体育讯　　孔蒂入主切尔西后，蓝军在8月的3场英超联赛中取得了全胜的成绩。然而，进入9月，意大利人突然发现在英超中并不那么容易拿分了。在上周末客场战平斯旺西之后，今天切尔西又在主场输给了利物浦。<a href=\"http://match.lottery.sina.com.cn/football/ai/base?m_id=3652197\" target=\"_blank\">[小炮准确命中利物浦客胜]</a><a href=\"http://lottery.sina.com.cn/ai/football/\" target=\"_blank\">[查看今日众豪门推荐]</a></p><p>　　在切尔西的失利中，第一粒丢球被视为最大的问题。可以看到，在库蒂尼奥起球时，切尔西的后点的防守球员都已经没了踪影，后插上的洛夫伦轻松将球打进。而在洛夫伦射门的瞬间，利物浦在切尔西的禁区内形成了4打1的局面。</p><p>　　对于这个球的防守，BBC评论道：“我确信在传中时利物浦一名在后门柱包抄的球员越位了，但他正在后退，而洛夫伦没越位，这次防守真的很烂。”。而做客BBC的名宿杰纳斯则充满戏谑的问道：“你要在后面漏掉多少人（才够）？”</p><p><img src=\"http://n.sinaimg.cn/sports/transform/20160917/f01w-fxvyzus1936003.gif\" alt=\"看看利物浦在切尔西禁区内的奇葩4打1\" data-link=\"\"/><span class=\"img_descr\">看看利物浦在切尔西禁区内的奇葩4打1</span></p><p>　　或许，此前切尔西的进攻掩盖了蓝军大部分的问题，尤其是防守。不过，当切尔西在那么简单的一次定位球防守中出现如此低级的失误时，防守问题无疑\r\n彻底的暴露了大家的眼皮底下。本赛季，切尔西队内最大的隐患，莫过于后防线。由于上半年祖玛的受伤，后防线闹起了人荒，引进的阿隆索，只抵消了巴巴-拉赫\r\n曼的离队；而且，随着几名主力特里、卡希尔和伊万诺维奇年龄的增大，即便他们的状态不出现下滑，受伤的几率仍然很大。果然，上周末做客斯旺西，特里的受伤\r\n影响到了今天的比赛。<strong>切尔西防守端的这颗暗雷终于被引爆了。</strong></p><p>　　顶替特里出场的，是回到蓝军的大卫-路易斯。表面上看，切尔西当初以5000万英镑的价格将路易斯卖给巴黎圣日耳曼，如今又用3500万买回，\r\n蓝军是赚了，但事实呢？路易斯依然是那个“誉满天下，谤满天下”的蓬蓬头，他的优点和缺点依然同样鲜明。就以切尔西的第一粒失球为例，路易斯和左边后卫阿\r\n斯皮利奎塔之间，根本没有默契可言。</p><p><img src=\"http://n.sinaimg.cn/sports/transform/20160917/SpSH-fxvyqvy6548037.jpg\" alt=\"路易斯拼到血染赛场，但他依然受到了批评\" data-link=\"\"/><span class=\"img_descr\">路易斯拼到血染赛场，但他依然受到了批评</span></p><p>　　goal.com赛后只为大卫-路易斯打出了4分，这是切尔西全队的最低分。该媒体评价道：“他的出场，使得切尔西后防线让人异常紧张。他对自\r\n己的定位是，经常参与球队控球，但这本是他不必要承担的责任。事实上，他在进攻端也没有贡献。”超低的评分和略带贬义的评价，无疑说明了大卫-路易斯回归\r\n后首秀的不及格。</p><p><img src=\"http://n.sinaimg.cn/sports/transform/20160917/Aq5i-fxvyqwa3311699.jpg\" alt=\"科斯塔传球次数全场第一，有点“不务正业”\" data-link=\"\"/><span class=\"img_descr\">科斯塔传球次数全场第一，有点“不务正业”</span></p><p>　　在赛后的技术统计中，路易斯传球66次，是全场最多的；而他的射门数，则也达到了3次，是全队最多的。这样的数据，对于一名中后卫而言，显然非常不正常。人们不禁要问，切尔西买回来的，到底是切尔西防线问题的解药还是毒药呢？</p><p>　　（比切多尔）</p><p><br/></p>',1474101826,'baagee','1',7);
/*!40000 ALTER TABLE `baagee_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baagee_category`
--

DROP TABLE IF EXISTS `baagee_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baagee_category` (
  `cat_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL DEFAULT '' COMMENT '分类名称',
  `cat_title` varchar(255) NOT NULL DEFAULT '',
  `cat_keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键词',
  `cat_description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `cat_views` int(11) NOT NULL DEFAULT '0' COMMENT '查看次数',
  `cat_order` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `cat_pid` varchar(255) NOT NULL DEFAULT '0' COMMENT '父级ID',
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='文章分类表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baagee_category`
--

LOCK TABLES `baagee_category` WRITE;
/*!40000 ALTER TABLE `baagee_category` DISABLE KEYS */;
INSERT INTO `baagee_category` VALUES (1,'新闻','经过风雨过后','的手感挺好的是','新博文收到个婚礼当天发货\"\"',0,4,'0'),(2,'笑话','fg 的方法给人打工','的发挥更好的','海洋领土一天了',0,2,'0'),(3,'娱乐','了的一天了','乐意让她','二元论',0,3,'0'),(4,'新浪新闻','而已特里','儿童乐园','乐意让她',3,5,'1'),(5,'腾讯新闻','56876','879678','98708',38,3,'1'),(6,'111娱乐',' 的发挥更好的','了一天了','了一趟',0,2,'3'),(7,'222娱乐','的还有两天','而已论坛','54龙应台4 ',1,3,'3'),(8,'爆慢笑话','的沙发上的','大厦股份都是','阿桑的歌挺合适的',0,1,'2'),(10,'百度新闻','的三个地方','动态规划法','突然关门了',0,4,'1'),(11,'xixi笑话','的三国塔防','电风扇广泛','沟通和社会不',0,3,'2'),(12,'哈哈笑话·','围观忒厉害了','是电话沟通了','好了改好了',0,2,'2');
/*!40000 ALTER TABLE `baagee_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baagee_config`
--

DROP TABLE IF EXISTS `baagee_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baagee_config` (
  `conf_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `conf_title` varchar(50) NOT NULL COMMENT '配置标题',
  `conf_name` varchar(50) NOT NULL COMMENT '配置名称',
  `conf_value` text NOT NULL,
  `conf_order` int(11) NOT NULL COMMENT '排序',
  `conf_tips` varchar(255) NOT NULL COMMENT '说明',
  `field_type` varchar(50) NOT NULL COMMENT '字段类型',
  `field_value` varchar(255) NOT NULL COMMENT '字段值',
  PRIMARY KEY (`conf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baagee_config`
--

LOCK TABLES `baagee_config` WRITE;
/*!40000 ALTER TABLE `baagee_config` DISABLE KEYS */;
INSERT INTO `baagee_config` VALUES (2,'不知道','不知道','222',4,'不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道不知道','input','1|开启,0|关闭'),(4,'网站状态','web_status','0',5,'网站是否能够访问','radio','1|开启,0|关闭'),(5,'新闻首页','web_title','新闻首页',0,'新闻首页','input',''),(6,'版权信息','web_copy','Design by <a href=\'http://baagee.top\' target=\'_blank\'>BaAGee</a> | Copyright © 1996 - 2014 SINA Corporation, All Rights Reserved',0,'版权信息','textarea','');
/*!40000 ALTER TABLE `baagee_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baagee_links`
--

DROP TABLE IF EXISTS `baagee_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baagee_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '友情链接名称',
  `link_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '友情链接标题',
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '友情链接url',
  `link_order` int(11) NOT NULL DEFAULT '0' COMMENT '友情链接排序',
  PRIMARY KEY (`link_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baagee_links`
--

LOCK TABLES `baagee_links` WRITE;
/*!40000 ALTER TABLE `baagee_links` DISABLE KEYS */;
INSERT INTO `baagee_links` VALUES (1,'你爸爸','你爸爸','http://www.nibaba.com',5),(2,'你妈妈','你妈妈','http://www.nimama.com',3),(3,'你姐姐','你姐姐','http://www.nijiejie.com',1),(4,'你弟弟','你弟弟','http://www.nididiiejie.com',2);
/*!40000 ALTER TABLE `baagee_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baagee_migrations`
--

DROP TABLE IF EXISTS `baagee_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baagee_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baagee_migrations`
--

LOCK TABLES `baagee_migrations` WRITE;
/*!40000 ALTER TABLE `baagee_migrations` DISABLE KEYS */;
INSERT INTO `baagee_migrations` VALUES ('2016_09_16_172522_create_links_table',1),('2016_09_16_192649_create_navs_table',2);
/*!40000 ALTER TABLE `baagee_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baagee_navs`
--

DROP TABLE IF EXISTS `baagee_navs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baagee_navs` (
  `nav_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nav_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单链接名称',
  `nav_alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单链接标题',
  `nav_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单链接url',
  `nav_order` int(11) NOT NULL DEFAULT '0' COMMENT '菜单链接排序',
  PRIMARY KEY (`nav_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baagee_navs`
--

LOCK TABLES `baagee_navs` WRITE;
/*!40000 ALTER TABLE `baagee_navs` DISABLE KEYS */;
INSERT INTO `baagee_navs` VALUES (2,'慢生活','Life','http://tyiutyoiyiu',5),(3,'首页','Protal','http://larblog.com',0),(4,'关于我','About','http://',6),(5,'碎言碎语','Doing','http://',3),(6,'模板分享','Share','http://',2),(7,'学无止境','Learn','http://',1),(8,'留言版','Gustbook','http://',4);
/*!40000 ALTER TABLE `baagee_navs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `baagee_user`
--

DROP TABLE IF EXISTS `baagee_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `baagee_user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(55) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '密码',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `baagee_user`
--

LOCK TABLES `baagee_user` WRITE;
/*!40000 ALTER TABLE `baagee_user` DISABLE KEYS */;
INSERT INTO `baagee_user` VALUES (1,'admin','eyJpdiI6IlhxZ3lJV25VXC9scjViNXh0bklBZmtnPT0iLCJ2YWx1ZSI6Iko4VVc1cVRzZGJyTHpvbzAwZWxZcTcwdmlnZnFoc05PQlVZc1wvdUdMZTlpbWs0OXkzdjl6TGp4NmE4akxtczlkIiwibWFjIjoiMjgzMTljNTAxMWNjZDkxZDAyNDllMWQzOTQyMWE5OTdiOGQ5ZTU5MzMxNWYyYzlhNWFlMTdlOTVjMGQzYzAwMiJ9');
/*!40000 ALTER TABLE `baagee_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-17 18:11:52
